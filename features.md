# conf / mpv

## Frame drops

Check for frame drops inside mpv with ``i``. Also notice the frame timings, with
16 milliseconds average per frame you can do 62.5fps. To be safe try to get
around 15 milliseconds per frame on the busy parts.

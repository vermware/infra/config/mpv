#!/bin/sh

# XXX: This repository differs from the usual configuration as it can be run as
# non-root users. To avoid random privilege increasing actions the dependency
# resolution was moved to a separate file.

set -eu

skipinstall="${1:-}"

# Install package if the test expression fails.
# $1 Expression that, if false, will install package.
# $2 Package to install.
installdep()
(
	if ! eval "$1"; then
		if [ "$skipinstall" ]; then
			exit 1
		fi

		apt-get -qy install "$2"
	fi
)

installdep 'hash mpv' mpv

#!/bin/sh

set -eu
umask 0077

hash m4

host="$1"

. src/config.sh
[ -e "host/$host/config.sh" ] && . "host/$host/config.sh"

[ -d build ] && rm -r -- build
mkdir -- build

shaderconf="host/$host/shaderconf"

m4 -EP \
	-D "__ALANG__=\`$alang'" \
	-D "__AO__=\`$ao'" \
	-D "__AUDIO_CHANNELS__=\`$audio_channels'" \
	-D "__CACHE_SECS__=\`$cache_secs'" \
	-D "__CORRECT_DOWNSCALING__=\`$correct_downscaling'" \
	-D "__CSCALE__=\`$cscale'" \
	-D "__DSCALE__=\`$dscale'" \
	-D "__FBO_FORMAT__=\`$fbo_format'" \
	-D "__HWDEC__=\`$hwdec'" \
	-D "__INTERPOLATION__=\`$interpolation'" \
	-D "__SAVE_POSITION_ON_QUIT__=\`$save_position_on_quit'" \
	-D "__SCALE__=\`$scale'" \
	-D "__SCREENSHOT_DIRECTORY__=\`$screenshot_directory'" \
	-D "__SCREENSHOT_HIGH_BIT_DEPTH__=\`$screenshot_high_bit_depth'" \
	-D "__SCREENSHOT_PNG_COMPRESSION__=\`$screenshot_png_compression'" \
	-D "__SHADERCONF__=\`$shaderconf'" \
	-D "__SIGMOID_UPSCALING__=\`$sigmoid_upscaling'" \
	-D "__VIDEO_SYNC__=\`$video_sync'" \
	-D "__VOLUME_MAX__=\`$volume_max'" \
	-- src/mpv.conf \
	>build/mpv.conf

mkdir -- build/script-opts
m4 -EP \
	-D "__OSC_LAYOUT__=\`$osc_layout'" \
	-- src/script-opts/osc.conf \
	>build/script-opts/osc.conf

[ -d "host/$host/shaders" ] && cp -LR -- "host/$host/shaders" build

cp -LR -- \
	src/scripts \
	build

#!/bin/sh

set -eu

./deps.sh skip

build="$1"
install_root="${INSTALL_ROOT:-}"

# Sanity check.
if [ "$(id -u)" -eq 0 ]; then
	[ "$install_root" ] || exit 1
fi

[ -f "$build/mpv.conf" ] || exit 1

# Config.
[ -d ~/.config/mpv ] || mkdir -- ~/.config/mpv
chmod -- 0700 ~/.config/mpv

cp -pR -- "$build/." ~/.config/mpv
